<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$res = array();
$headers = getallheaders();

if (isset($_GET['berto'])) {

    $token = bin2hex(random_bytes(20));
    $public_key = file_get_contents('./wberto.pub', true);
    openssl_public_encrypt($token, $encrypted, $public_key);

    echo "This is the encrypted text: $encrypted\n\n";

    $encrypted_hex = bin2hex($encrypted);
    echo "This is the encrypted_hex token: $encrypted_hex\n\n";
    echo "This is the original: $token\n\n";
    die();

    foreach ($_SERVER as $key_name => $key_value) {
        $res[$key_name] = $key_value;

        //echo $key_name . " = " . $key_value . "<br>";
    }
    //echo json_encode($res);
    die();
}

if (!isset($_GET['go'])) {
    http_response_code(404);
    echo 'Not Found';
    die();
}

$request = $_GET['go'];

switch ($request) {
    case '':
        require __DIR__ . '/views/index.php';
        break;
    case 'test':
        require __DIR__ . '/views/test.php';
        break;
    case 'pippo':
        require __DIR__ . '/views/pippo.php';
        break;
    default:
        require __DIR__ . '/views/404.php';
        break;
}

<?php
class Database
{
    private $host = "localhost.db"; // mysql / Sqlite db file
    private $db_name = "";
    private $username = "";
    private $password = "";
    public $conn;

    function __construct()
    {
        $this->connectDb();
    }

    private function connectDb()
    {
        $this->conn = null;
        try {
            $this->conn = new PDO("sqlite:" . $this->host);
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
        //return $this->conn;
    }

    public function qFetch($sql, $all = true)
    {
        $conn = $this->conn;
        $stmt = $conn->prepare($sql);
        //$stmt->bindParam(':table', 'prova', PDO::PARAM_STR);
        $stmt->execute();
        if ($all == true) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        return $result;
    }
}
